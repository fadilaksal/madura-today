$('.btn-status').click(function () {
	let id_user = $(this).data('id');
	// console.log(id_user);
	// axios.post('update_status', {
	// 	id: id_user,
	// 	// lastName: 'Flintstone'
	// })
	// .then(function (response) {
	// 	console.log(response);
	// })
	// .catch(function (error) {
	// 	console.log(error);
	// });
	axios({
		method: 'post',
		url: 'update_status',
		data: {
			id: id_user,
		}
	}).then(function(response) {
		let tipe = 'error';
		if(response.status){
			tipe = 'success';
		}
		swal('Sukses!', response.messages, tipe).then(function(){
			location.reload();
		});
	});
});
