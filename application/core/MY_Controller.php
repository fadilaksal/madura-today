<?php

class MY_Controller extends MX_Controller {

    var $data;
    var $user;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->data['website_name'] = 'Sim LBB';
        $this->data['user'] = $this->getUser();
        // print_r($this->data['user']);
        // die();
        $this->user = $this->getUser();
    }

    public function getLayout($param1 = 'public') {
        if($param1 == 'admin'){
            return config_item('template_admin');
        } else{
            return config_item('template_public');
        }
    }

    public function getUser() {
        return $this->auth->user()->row();
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function unique_field_name($field_name) {
	    return 's'.substr(md5($field_name),0,8); //This s is because is better for a string to begin with a letter and not with a number
    }

    public function uploadFile($location = '', $field = '')
    {
        $config['upload_path']          = $location;
        $config['allowed_types']        = 'pdf|PDF';
        $config['remove_spaces']        = true;
        $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($field)){
            $error  = array('error' => $this->upload->display_errors());
            $return = array('status' => false, 'message' => $error);
        } else{
            $fileName  = $this->upload->data('file_name');

            $return = array('status' => true, 'name' => $fileName);
        }

        return $return;
    }

    public function uploadImage($location = '', $field = '')
    {
        $config['upload_path']          = $location;
        $config['allowed_types']        = '*';
        $config['remove_spaces']        = true;
        $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($field)){
            $error  = array('error' => $this->upload->display_errors());
            $return = array('status' => false, 'message' => $error);
        } else{
            $imageName  = $this->upload->data('file_name');

            $this->load->library('Image_moo');
            $file_uploaded = $field . $imageName;
            $this->image_moo->load($file_uploaded)->resize(800,680)->save($file_uploaded,true);

            $return = array('status' => true, 'name' => $imageName);
        }

        return $return;
    }

}
