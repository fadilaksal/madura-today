<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {
	function __construct()
    {
        parent::__construct();
        $this->parser->setModule("page");
				$this->data['user'] = $this->user;
    }
    /**
     * Default Method
     *
     * @return void
     */
    public function index()
    {
				$this->home();
    }

		public function home()
		{
			$top_news = $this->berita_m->with('author_id')->top_news(5)->get_many_by(array('status'=>'rilis'));
			$slider 	= $this->berita_m->with('author_id')->top_news(5)->get_many_by(array('status'=>'rilis'));
			$latest 	= $this->berita_m->with('author_id')->latest_news(11)->get_many_by(array('status'=>'rilis'));
			$citizen 	= $this->berita_m->with('author_id')->latest_news(5)->get_many_by(array('status'=>'rilis', 'kategori'=>'citizen_jurnalism'));
			// print_r($top_news);

			$this->data['top_news'] = $top_news;
			$this->data['slider'] 	= $slider;
			$this->data['latest']		= $latest;
			$this->data['page']			= 'Home';
			$this->data['citizen']	= $citizen;

			$this->data['css_include'] = array(
				assets_url() . "bootstrap4/dist/css/bootstrap.css",
				assets_url() . "avision/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/animate.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.theme.default.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.css",
				assets_url() . "avision/plugins/font-awesome-4.7.0/css/font-awesome.min.css",
				assets_url() . "avision/styles/main_styles.css",
				assets_url() . "avision/styles/responsive.css",
			);

			$this->data['js_include'] = array(
				assets_url() . "avision/js/jquery-3.2.1.min.js",
				assets_url() . "avision/styles/bootstrap4/popper.js",
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.js",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.js",
				assets_url() . "avision/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.js",
				assets_url() . "avision/plugins/easing/easing.js",
				assets_url() . "avision/plugins/masonry/masonry.js",
				assets_url() . "avision/plugins/masonry/images_loaded.js",
				assets_url() . "avision/js/custom.js",
			);

			$this->data["main_content"] = $this->smarty->view("home.html", $this->data, true);
			$this->smarty->display(APPPATH . 'views\frontend\main_templates\layout.html', $this->data);
		}

		public function kategori($kategori='')
		{

			$latest 	= $this->berita_m->with('author_id')->latest_news(9)->get_many_by(array('status'=>'rilis', 'kategori'=>$kategori));
			$top_news = $this->berita_m->with('author_id')->top_news(10)->get_many_by(array('status'=>'rilis', 'kategori'=>$kategori));
			$latest_count = count($latest);
			// print_r($latest_count);
			// print_r($latest);
			$this->data['latest'] 	= $latest;
			$this->data['latest_count'] = $latest_count;
			$this->data['popular']	= $top_news;
			$this->data['page']			= ucfirst($kategori);

			$this->data['css_include'] = array(
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.css",
				assets_url() . "avision/plugins/font-awesome-4.7.0/css/font-awesome.min.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.theme.default.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/animate.css",
				assets_url() . "avision/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.css",
				assets_url() . "avision/styles/category.css",
				assets_url() . "avision/styles/category_responsive.css",
			);

			$this->data['js_include'] = array(
				assets_url() . "avision/js/jquery-3.2.1.min.js",
				assets_url() . "avision/styles/bootstrap4/popper.js",
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.js",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.js",
				assets_url() . "avision/plugins/easing/easing.js",
				assets_url() . "avision/plugins/masonry/masonry.js",
				assets_url() . "avision/plugins/parallax-js-master/parallax.min.js",
				assets_url() . "avision/js/category.js",
			);

			$this->data["main_content"] = $this->smarty->view("category.html", $this->data, true);
			$this->smarty->display(APPPATH . 'views\frontend\main_templates\layout.html', $this->data);
		}

		public function berita($param1='', $param2='')
		{
			$berita = $this->berita_m->with('author_id')->get($param1);
			if ($berita == null) {
				redirect('/');
			}

			$komentar 			= $this->komentar_m->with('user')->get_many_by(array('berita_id'=>$berita->id));
			$update 				= $this->berita_m->update($param1, array('view_count'=>$berita->view_count+1));
			$berita_author 	= $this->berita_m->with('author_id')->latest_news(3)->get_many_by(array('status'=>'rilis', 'author_id'=>$berita->author_id->id));
			$top_news 			= $this->berita_m->with('author_id')->top_news(10)->get_many_by(array('status'=>'rilis'));
			$berita->komentar = $komentar;

			$this->data['komentarCount'] = count($komentar);
			$this->data['berita_author'] = $berita_author;
			$this->data['berita'] 	= $berita;
			$this->data['popular'] 	= $top_news;
			$this->data['user'] 		= $this->user;
			$this->data['page'] 		= 'Berita';

			$this->data['css_include'] = array(
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.css",
				assets_url() . "avision/plugins/font-awesome-4.7.0/css/font-awesome.min.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.theme.default.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/animate.css",
				assets_url() . "avision/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.css",
				assets_url() . "avision/styles/post.css",
				assets_url() . "avision/styles/post_responsive.css",
			);

			$this->data['js_include'] = array(
				assets_url() . "avision/js/jquery-3.2.1.min.js",
				assets_url() . "avision/styles/bootstrap4/popper.js",
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.js",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.js",
				assets_url() . "avision/plugins/easing/easing.js",
				assets_url() . "avision/plugins/masonry/masonry.js",
				assets_url() . "avision/plugins/parallax-js-master/parallax.min.js",
				assets_url() . "avision/js/post.js",
			);

			$this->data["main_content"] = $this->smarty->view("post.html", $this->data, true);
			$this->smarty->display(APPPATH . 'views\frontend\main_templates\layout.html', $this->data);
		}

		public function redaksi()
		{
			$this->data['page'] = "Redaksi";

			$this->data['css_include'] = array(
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.css",
				assets_url() . "avision/plugins/font-awesome-4.7.0/css/font-awesome.min.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.theme.default.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/animate.css",
				assets_url() . "avision/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.css",
				assets_url() . "avision/styles/regular.css",
				assets_url() . "avision/styles/regular_responsive.css",
			);

			$this->data['js_include'] = array(
				assets_url() . "avision/js/jquery-3.2.1.min.js",
				assets_url() . "avision/styles/bootstrap4/popper.js",
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.js",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.js",
				assets_url() . "avision/plugins/easing/easing.js",
				assets_url() . "avision/plugins/masonry/masonry.js",
				assets_url() . "avision/plugins/parallax-js-master/parallax.min.js",
				assets_url() . "avision/js/regular.js",
			);

			$this->data["main_content"] = $this->smarty->view("redaksi.html", $this->data, true);
			$this->smarty->display(APPPATH . 'views\frontend\main_templates\layout.html', $this->data);
		}

		public function pedoman()
		{

			$this->data['page'] = "Pedoman";
			$this->data['css_include'] = array(
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.css",
				assets_url() . "avision/plugins/font-awesome-4.7.0/css/font-awesome.min.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.theme.default.css",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/animate.css",
				assets_url() . "avision/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.css",
				assets_url() . "avision/styles/regular.css",
				assets_url() . "avision/styles/regular_responsive.css",
			);

			$this->data['js_include'] = array(
				assets_url() . "avision/js/jquery-3.2.1.min.js",
				assets_url() . "avision/styles/bootstrap4/popper.js",
				assets_url() . "avision/styles/bootstrap4/bootstrap.min.js",
				assets_url() . "avision/plugins/OwlCarousel2-2.2.1/owl.carousel.js",
				assets_url() . "avision/plugins/easing/easing.js",
				assets_url() . "avision/plugins/masonry/masonry.js",
				assets_url() . "avision/plugins/parallax-js-master/parallax.min.js",
				assets_url() . "avision/js/regular.js",
			);

			$this->data["main_content"] = $this->smarty->view("pedoman.html", $this->data, true);
			$this->smarty->display(APPPATH . 'views\frontend\main_templates\layout.html', $this->data);
		}

		public function komentar()
		{
			$content 		= $this->input->post('content');
			$berita_id 	= $this->input->post('berita_id');
			if ($this->user == null) {
				redirect('/');
			}
			if ($content != null && $berita_id != null && (int)$berita_id > 0) {
				$this->komentar_m->insert(array('content'=>$content, 'berita_id'=>$berita_id, 'user_id'=>$this->user->id));
			}
			redirect('/');
		}
}
