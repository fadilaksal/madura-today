<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->parser->setModule("dashboard");
        $this->data['page'] = 'Dashboard';
        if(!isset($this->user) || $this->user == null){
            header("location:auth");
        }
    }
    /**
     * Default Method
     *
     * @return void
     */
    public function index()
    {
        // $this->data["jumlahMember"] = $this->member_m->count_all();
        $this->data["jumlahAdmin"] = $this->user_m->count_all();
        // $this->data["jumlahPesan"] = $this->pesan_m->count_all();

        $this->data["main_content"] = $this->smarty->view("dashboard.html", $this->data, true);
        $this->smarty->display($this->getLayout('admin'), $this->data);
    }
}
