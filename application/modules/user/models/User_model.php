<?php
class User_model extends MY_Model {
    public function __construct()
    {
        parent::__construct();
        $this->_table = 'users';
    }

  public function _select($param1 = '*'){ 
    $this->db->select($param1); 
    return $this; 
  } 
 
  public function _order_by($param1 = '', $param2 = ''){ 
    $this->db->order_by("$param1 $param2"); 
    return $this; 
  }

  public function _with_level()
  {
    $this->db->select("users.*, b.name as level");
    $this->db->join("users_groups a", "a.user_id = users.id", "left");
    $this->db->join("groups b", "b.id = a.group_id", "left");
    return $this;
  }

  public function _not($id)
  {
    $this->db->where("users.id != $id and users.grup != 'konseptor'");
    return $this;
  }

  public function get_field(){
    return $this->db->list_fields('users');
  }
}