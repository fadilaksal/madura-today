<?php

class User extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->parser->setModule("user");
        // if($this->user == null){
        //
        // }

        if(!isset($this->user) || $this->user == null){
            redirect('auth');
        }
        $this->data['page'] = "User";
        $this->data['menu_admin'] = true;
    }

    public function index() {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();
        $crud->set_table('users');
        $crud->unset_texteditor('alamat');

        if($this->user->tipe != 'administrator'){
            $crud->where('id', $this->user->id);
            $crud->unset_add();
            $crud->unset_delete();
            $crud->field_type('tipe', 'invisible');
            $crud->field_type('co', 'invisible');
            $crud->field_type('active', 'invisible');
        }else{
            $crud->field_type('active','dropdown', array('1' => 'Active', '0' => 'Not Active'));
        }

        $crud->columns('username', 'nama', 'tipe');
        // $crud->display_as('Nama Depan', 'Nama Belakang', 'Username', 'Daftar pada');

        $crud->required_fields('username', 'nama', 'alamat');

        $crud->field_type('password', 'password')
                ->field_type('ip_address', 'invisible')
                ->field_type('salt', 'invisible')
                ->field_type('created_on', 'invisible');

        $crud->unset_add_fields('email','activation_code', 'forgotten_password_code', 'forgotten_password_time','remember_code', 'last_login');

        if($crud->getstate() == "edit"){
            $crud->unset_edit_fields('activation_code', 'forgotten_password_code', 'forgotten_password_time','remember_code', 'last_login', 'email');
            $crud->field_type('password', 'invisible')
                    ->field_type('ip_address', 'invisible')
                    ->field_type('salt', 'invisible')
                    ->field_type('created_on', 'invisible');
            // $crud->required_fields('username', 'active', 'nama', 'phone');
        }else if($crud->getState()=="insert_validation"){
            $crud->set_rules('password', 'Password','required|min_length[6]')
                ->set_rules('nama', 'First Name','required|trim');
                // ->set_rules('phone', 'No. Tepon','required');
        }else if($crud->getState()=="update_validation"){
            $crud->set_rules('nama', 'First Name','required|trim');
                // ->set_rules('phone', 'No. Tepon','required');
        }else if($crud->getState()=='read'){
          $crud->unset_fields('password', 'salt', 'activation_code', 'forgotten_password_code', 'forgotten_password_time', 'remember_code', 'created_on', 'active');
          $crud->callback_read_field('last_login', array($this, '_callback_read_lastlogin'));
          $crud->callback_read_field('active', array($this, 'callback_read_active'));
        }

        $crud->set_field_upload('image', 'assets/uploads/images-member/', 'jpg|jpeg|png');

        $crud->callback_insert(array($this, 'add_user'));

        $crud->add_action('Ubah Password',  base_url() . 'assets/grocery_crud/themes/flexigrid/css/images/key.png', 'user/change_password', 'ui-icon-image');

        $this->data['gc_data'] = $crud->render();

        $this->smarty->display($this->getLayout('admin'), $this->data);
    }

    public function group() {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();
        $crud->set_table('users_groups');
        $crud->set_relation("group_id", "groups", "name");
        $crud->set_relation("user_id", "users", "username");

        $this->data['gc_data'] = $crud->render();

        $this->smarty->display($this->getLayout(), $this->data);
    }

    function add_user($values){
        if($this->form_validation->run()){
            $password = $values['password'];
            $username = $values['username'];
            $additional_data = array(
                'tipe' => $values['tipe'],
                'co' => $values['co'],
                'nama' => $values['nama'],
                'tgl_lahir' => $values['tgl_lahir'],
                'jenis_kelamin' => $values['jenis_kelamin'],
                'alamat' => $values['alamat'],
                'telepon' => $values['telepon'],
                'created_on' => date('Y-m-d H:i:s')
            );

            if($this->auth->register($username, $password, "", $additional_data, array(1))){
                // die("test");
                return true;
            }else{
                // die($this->auth->errors());
                return false;
                //return $this->auth->errors();
            }
        }else{
            return false;
        }
    }

    function change_password() {
        $crud = new Grocery_CRUD();
        $crud->set_table('users');
        $crud->unset_list();
        $crud->unset_add();
        $crud->unset_back_to_list();
        $crud->unset_jquery();

        $crud->fields('password', 'verify_password');
        $crud->field_type('password', 'password')->field_type('verify_password', 'password');

        $crud->required_fields('password', 'verify_password');
        $crud->set_rules('password', 'Password', 'trim|required|matches[verify_password]|min_length[6]');

        $id = (int) $this->uri->segment(3);
        if($id<0){
            redirect('user');
        }

        $crud->callback_update(array($this, 'update_pass'));

        try {
            $this->data["gc_data"] = $crud->render();
        } catch (Exception $ex) {
            if ($ex->getCode() == 14) {
                redirect('user/change_password/index/edit/' . $id);
            }else{
                redirect('user');
            }
        }

        $this->smarty->display($this->getLayout('admin'), $this->data);
    }

    function update_pass($values, $primary_key){
        $data = array('password' => $values['password']);
        if($this->auth->update($primary_key, $data)){
            return true;
        }else{
            return false;
        }
    }

    function callback_read_active($values){
        if($values == 1){
            return 'Active';
        }else{
            return 'Non Active';
        }
    }

    public function _callback_read_lastlogin($value)
    {
      return date('d-m-Y H:i:s', $value);
    }
}
