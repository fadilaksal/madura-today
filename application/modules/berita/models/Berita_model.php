<?php
class Berita_model extends MY_Model {
  public $belongs_to = array('author_id' => array('model' => 'user/user_model', 'primary_key' => 'author_id'));
  public $has_many = array('komentar' => array('model' => 'berita/komentar_model', 'primary_key' => 'berita_id'));

  public function __construct()
  {
      parent::__construct();
      $this->_table = 'berita';
  }

  public function _select($param1 = '*'){
    $this->db->select($param1);
    return $this;
  }

  public function _limig($limit='', $offset='')
  {
    $this->db->limit($limit, $offset);
    return $this;
  }

  public function top_news($limit = 1)
  {
    $this->db->order_by('view_count', 'desc');
    $this->db->limit($limit);
    return $this;
  }

  public function popular_news($limit = 1)
  {
    $this->db->order_by('liked_count', 'desc');
    $this->db->limit($limit);
    return $this;
  }

  public function latest_news($limit=1)
  {
    $this->db->order_by('created_at', 'desc');
    $this->db->limit($limit);
    return $this;
  }

  public function _order_by($param1 = '', $param2 = ''){
    $this->db->order_by("$param1 $param2");
    return $this;
  }
}
