<?php
class Komentar_model extends MY_Model {
  public $belongs_to = array( 'berita_id' => array('model' => 'berita/berita_model', 'primary_key' => 'berita_id'),
                              'user' => array('model' => 'user/user_model', 'primary_key' => 'user_id'));

  public function __construct()
  {
      parent::__construct();
      $this->_table = 'komentar';
  }

  public function _order_by($param1 = '', $param2 = ''){
    $this->db->order_by("$param1 $param2");
    return $this;
  }
}
