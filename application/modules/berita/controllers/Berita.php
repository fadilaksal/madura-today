<?php

class Berita extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->parser->setModule("berita");
        // if($this->user == null){
        //
        // }

        if(!isset($this->user) || $this->user == null){
            redirect('auth');
        }
        $this->data['page'] = "Berita";
        $this->data['berita_menu'] = true;
    }

    public function index()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();
        $crud->set_table('berita');
        if ($crud->getState() != 'add' && $crud->getState() != 'edit') {
          $crud->set_relation('author_id', 'users', 'nama');
        }
        if($this->user->tipe == 'citizen_jurnalism' || $this->user->tipe == 'wartawan'){
            $crud->where(array('author_id'=>$this->user->id));
        } else{
            $crud->where('status','rilis');
        }

        if($this->user->tipe == 'citizen_jurnalism'){
            $crud->field_type('kategori', 'invisible');
        }

        if ($this->user->tipe != 'pemred' && $this->user->tipe != 'administrator') {
          // $crud->unset_add();
          $crud->unset_edit();
          $crud->unset_delete();
        }

        $crud->columns('author_id','title', 'kategori', 'status', 'created_at');
        $crud->field_type('liked_count', 'invisible');
        $crud->field_type('view_count', 'invisible');
        $crud->field_type('status', 'invisible');
        $crud->field_type('author_id', 'invisible');

        $crud->field_type('created_at', 'invisible');

        $crud->set_field_upload('image', 'assets/uploads/images-news');
        $crud->callback_before_insert(array($this, '_callback_insert_berita'));

        $this->data['gc_data'] = $crud->render();

        $this->smarty->display($this->getLayout('admin'), $this->data);
    }

    public function verifikasi()
    {
        $crud = new grocery_CRUD();
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_jquery();
        $crud->set_table('berita');
        $crud->field_type('view_count', 'invisible');
        $crud->field_type('liked_count', 'invisible');
        $crud->field_type('created_at', 'invisible');

        if ($this->user->tipe == 'co') {
          $crud->unset_edit();
          $crud->where(array('status'=>'baru', 'kategori'=>$this->user->co));
          $crud->set_relation('author_id', 'users', 'nama');
        }elseif ($this->user->tipe == 'guide_keeper') {
          $crud->unset_edit();
          $crud->where('status', 'co');
          $crud->set_relation('author_id', 'users', 'nama');
        }elseif ($this->user->tipe == 'editor') {
          $crud->field_type('kategori', 'invisible');
          $crud->field_type('status', 'invisible');
          $crud->where('status', 'guide_keeper');
          $crud->set_relation('author_id', 'users', 'nama');
        }elseif ($this->user->tipe == 'pemred' || $this->user->tipe == 'administrator') {
          $crud->where('status', 'editor');
          $crud->set_relation('author_id', 'users', 'nama');
        }else if($this->user->tipe == 'wartawan' || $this->user->tipe == 'citizen_jurnalism'){
            $crud->field_type('kategori', 'invisible');
            $crud->field_type('status', 'invisible');
            $crud->field_type('author_id', 'invisible');
            $crud->where("status = 'baru' and author_id = ".$this->user->id,null,FALSE);
            // $crud->field_type();
        //   $crud->unset_edit();
        }else{
          redirect('auth');
        }


        if($this->user->tipe != 'wartawan' && $this->user->tipe != 'citizen_jurnalism'){
            $crud->columns('author_id','title', 'kategori', 'created_at');
            $crud->add_action( 'Verifikasi',  '', 'berita/proses_verifikasi/diterima', 'fas fa-check');
            $crud->add_action( 'Revisi',  '', 'berita/proses_verifikasi/revisi', 'fas fa-times ');
        }else{
            $crud->columns('author_id','title', 'kategori','status', 'created_at');
        }
        $crud->field_type('created_at', 'invisible');

        $crud->set_field_upload('image', 'assets/uploads/images-news');

        $this->data['gc_data'] = $crud->render();

        $this->smarty->display($this->getLayout('admin'), $this->data);

    }

    public function kategori($kategori='')
    {
      $crud = new grocery_CRUD();
      $crud->unset_jquery();
      $crud->set_table('berita');
      $crud->where('kategori', $kategori);

      if ($this->user->tipe != 'pemred' && $this->user->tipe != 'administrator') {
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
      }

      $crud->field_type('created_at', 'invisible');

      $crud->set_field_upload('image', 'assets/uploads/images-news');

      $this->data['gc_data'] = $crud->render();

      $this->smarty->display($this->getLayout('admin'), $this->data);
    }

    public function revisi()
    {
      $crud = new grocery_CRUD();
      $crud->unset_add();
      $crud->unset_delete();
      $crud->unset_jquery();
      $crud->set_table('berita');
      $crud->set_relation('author_id', 'users', 'nama');
      if ($this->user->tipe == 'co') {
        $crud->where(array('status'=>'revisi', 'kategori'=>$this->user->co));
      } else if($this->user->tipe == 'wartawan'){
        $crud->where(array('status'=>'revisi', 'author_id'=>$this->user->id));
      } else if($this->user->tipe == 'citizen_jurnalism'){
        $crud->where(array('status'=>'revisi', 'author_id'=>$this->user->id));
      } else{
        $crud->where(array('status'=>'revisi'));
      }

      if($this->user->tipe == 'editor'){
        $crud->unset_add();
        $crud->unset_delete();
      }elseif ($this->user->tipe != 'pemred' && $this->user->tipe != 'administrator' && $this->user->tipe != 'citizen_jurnalism' && $this->user->tipe != 'wartawan') {
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
      }

      $crud->columns('author_id','title', 'kategori', 'created_at');
      $crud->add_action( 'Kirim',  '', 'berita/proses_kirim', 'fas fa-check ');

      $crud->field_type('created_at', 'invisible');

      $crud->set_field_upload('image', 'assets/uploads/images-news');

      $this->data['gc_data'] = $crud->render();

      $this->smarty->display($this->getLayout('admin'), $this->data);
    }

    public function proses_verifikasi($status = '', $berita_id = 0)
    {
      if ($status == 'diterima') {
        if ($this->user->tipe == 'pemred') {
          $this->berita_m->update($berita_id, array('status'=>'rilis'));
        }else{
          $this->berita_m->update($berita_id, array('status'=>$this->user->tipe));
        }
        echo "<script>alert('Laporan Berhasil diverifikasi');</script>";
      }else if($status == 'revisi'){
        $this->berita_m->update($berita_id, array('status'=>'revisi'));
        echo "<script>alert('Status Laporan Berhasil diubah menjadi revisi');</script>";
      }
      echo "<script>window.location.href='" . base_url() . "berita/verifikasi';</script>";
    }

    public function proses_kirim($berita_id = 0)
    {
      if ($this->user->tipe == 'pemred') {
        $this->berita_m->update($berita_id, array('status'=>'rilis'));
      } else if($this->user->tipe == 'wartawan' || $this->user->tipe == 'citizen_jurnalism'){
        $this->berita_m->update($berita_id, array('status'=>'baru'));
      } else{
        $this->berita_m->update($berita_id, array('status'=>$this->user->tipe));
      }
      echo "<script>alert('Laporan Berhasil dikirim');</script>";

      echo "<script>window.location.href='" . base_url() . "berita/revisi';</script>";
    }

    public function _callback_insert_berita($postData)
    {
      $postData['status'] = 'baru';
      $postData['author_id'] = $this->user->id;
      if($this->user->tipe == 'citizen_jurnalism'){
          $postData['kategori'] = 'citizen_jurnalism';
      }
      return $postData;
    }
}
